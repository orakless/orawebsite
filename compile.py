#!/usr/bin/env python3

import json
import os
import shutil


def make_clean():
    shutil.rmtree("output")
    os.mkdir("output")


def save(path, file):
    with open(path, "w+") as f:
        f.write(file)


def import_data():
    with open("main.json", "r") as file:
        main = json.load(file)

    with open("projects.json", "r") as file:
        projects = json.load(file)

    with open("services.json", "r") as file:
        services = json.load(file)

    with open("main.css", "r") as file:
        css = file.read()

    return (main, projects, services, css)


def make_head(mainDict):
    def _make_open_graph_twitter(informations):
        return [f"  <meta property=\"{item[0]}\" content=\"{item[1]}\">" 
                for item in informations
                if item[1 != ""]]

    def _make_header(informations):
        head = ["<head>",
                "   <meta charset=\"utf-8\">",
                f"  <title>{informations['title']}</title>",
                "   <link href=\"main.css\" rel=\"stylesheet\">",
                "   <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"favicon.ico\">"]
                
        head += _make_open_graph_twitter(informations["opengraph"])
        head += _make_open_graph_twitter(informations["twitter"])
        head.append("</head>")
        return head

    return _make_header(mainDict)


def make_body(mainDict, projectsDict, servicesDict):
    def _make_description(informations):
        return ["<div id=\"presentation\">",
                f"<h2>{informations['name']}</h2>",
                f"<p>{informations['description']}</p>",
                "</div>"]
    def _make_projects(projects):
        projects_list = ["<div id=\"projects\">", "<h2>Projets</h2>"]
        for item in projects:
            tag_list = ["<span class=\"tag\">"+tag+"</span>" for tag in item['tags']]
            projects_list += ["<div class=\"project_title\">",
                                f"  <h3><a href=\"{item['link']}\">{item['name']}</a></h3>",
                                f"  <p>{item['description']}</p>",
                                f"  <p class=\"taglist\">{''.join(tag_list)}</p>",
                                f"</div>\n"]
        projects_list.append("</div>")
        return projects_list

    def _make_services(services):
        services_list = ["<div id=\"services\">", "<h2>Services</h2>"]
        for item in services:
            tag_list = ["<span class='tag'>"+tag+"</span>" for tag in item['tags']]
            services_list += ["<div class=\"service_title\">",
                                f"  <h3><a href=\"{item['link']}\">{item['name']}</a></h3>",
                                f"  <p>{item['description']}</p>",
                                f"  <p class=\"taglist\">{''.join(tag_list)}</p>",
                                f"</div>\n"]
        services_list.append("</div>")
        return services_list
    
    def _make_body(informations, prDict, srDict):
        body = ["<body>"]
        body += _make_description(informations)
        body += _make_projects(prDict)
        body += _make_services(srDict)
        body.append("</body>")
        return body
    
    return _make_body(mainDict, projectsDict, servicesDict)


def make_footer(mainDict):
    def _make_copyright(informations):
        return [informations["copyright"]]
    
    def _make_footer(informations):
        footer = ["<footer>"]
        if informations["footer"] != "":
            footer.append(informations["footer"])
        footer += _make_copyright(informations)
        footer.append("</footer>")
        return footer
    
    return _make_footer(mainDict)


def make_web_page(mainDict, projectsDict, servicesDict):
    webPage = ["<!DOCTYPE html>", "<html>"]
    webPage += make_head(mainDict)
    webPage += make_body(mainDict, projectsDict, servicesDict)
    webPage += make_footer(mainDict)
    webPage.append("</html>")
    return webPage


def export_web_page():
    data = import_data()
    web_page = make_web_page(data[0], data[1], data[2])
    make_clean()
    save("output/index.html", "\n".join(web_page))
    save("output/main.css", data[3])
    shutil.copy("favicon.ico", "output/favicon.ico")
    shutil.copytree("assets", "output/assets")

export_web_page()
